﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FreeWPIConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FreeWPI by Computer Team. Сборник программ с открытым исходным кодом. v 0.0.1 Closed-Beta Console");
            Console.WriteLine("Помощь и поддержка: https://discord.gg/Aj2tMcM");
        
            Console.WriteLine("Укажите путь для распаковки программ:");
            string patch = Console.ReadLine();
            if (Directory.Exists(patch) == false)
            {
                MessageBox.Show("Указанная директория не найдена!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
                //ГРАФИКА
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Раздел: Графика");
            Console.ResetColor();
            Console.WriteLine("Распаковать Inkscape? (yes/Enter - no)"); //INKSCAPE
            string uninkscape = Console.ReadLine();
            if (uninkscape == "yes")
            {
                string inkscape = patch + @"\InkscapePortable.exe";
                File.WriteAllBytes(inkscape, Properties.Resources.InkscapePortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = inkscape;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Krita? (yes/Enter - no)"); //KRITA
            string unkrita = Console.ReadLine();
            if (unkrita == "yes")
            {
                string krita = patch + @"\KritaPortable.exe";
                File.WriteAllBytes(krita, Properties.Resources.KritaPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = krita;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать ImageGlass? (yes/Enter - no)"); //IMAGE GLASS
            string unimgglass = Console.ReadLine();
            if (unimgglass =="yes")
            {
                string imgglass = patch + @"\ImageGlass.exe";
                File.WriteAllBytes(imgglass, Properties.Resources.ImageGlass); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = imgglass;              
                p.Start(); //распаковка архива              
            }

            Console.WriteLine("Распаковать Blender? (yes/Enter - no)"); //BLENDER
            string unblender = Console.ReadLine();
            if (unimgglass == "yes")
            {
                string blender = patch + @"\BlenderPortable.exe";
                File.WriteAllBytes(blender, Properties.Resources.BlenderPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = blender;
                p.Start(); //распаковка архива              
            }

            Console.WriteLine("Распаковать ShareX? (yes/Enter - no)"); //SHAREX
            string unsharex = Console.ReadLine();
            if (unimgglass == "yes")
            {
                string sharex = patch + @"\ShareX.exe";
                File.WriteAllBytes(sharex, Properties.Resources.ShareX); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = sharex;
                p.Start(); //распаковка архива              
            }

            Console.WriteLine("Распаковать Pencil2D? (yes/Enter - no)"); //Pencil2D
            string unpencil = Console.ReadLine();
            if (unpencil == "yes")
            {
                string pencil = patch + @"\Pencil2DPortable.exe";
                File.WriteAllBytes(pencil, Properties.Resources.Pencil2DPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = pencil;
                p.Start(); //распаковка архива              
            }

            Console.WriteLine("Распаковать ZCad? (yes/Enter - no)"); //ZCAD
            string unzcad = Console.ReadLine();
            if (unzcad == "yes")
            {
                string zcad = patch + @"\zcad.exe";
                File.WriteAllBytes(zcad, Properties.Resources.zcad); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = zcad;
                p.Start(); //распаковка архива              
            }



            //МЕДИА
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Раздел: Медиа");
            Console.ResetColor();

            Console.WriteLine("Распаковать mkvtoolnix? (yes/Enter - no)"); //mkvtoolnix
            string unmkvtool = Console.ReadLine();
            if (unmkvtool == "yes")
            {
                string mkvtn = patch + @"\mkvtoolnix-gui.exe";
                File.WriteAllBytes(mkvtn, Properties.Resources.mkvtoolnix_gui); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = mkvtn;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать AudaCity? (yes/Enter - no)"); //AUDACITY
            string unaudacity = Console.ReadLine();
            if (unaudacity =="yes")
            {
                string auda = patch + @"\audacity.exe";
                File.WriteAllBytes(auda, Properties.Resources.audacity); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = auda;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать ClipGrab? (yes/Enter - no)"); //CLIPGRAB
            string unclipgrab = Console.ReadLine();
            if (unclipgrab =="yes")
            {
                string clipg = patch + @"\clipgrab.exe";
                File.WriteAllBytes(clipg, Properties.Resources.clipgrab); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = clipg;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать LMMS? (yes/Enter - no)"); //LMMS
            string unlmms = Console.ReadLine();
            if (unlmms =="yes")
            {
                string lmms = patch + @"\LMMSPortable.exe";
                File.WriteAllBytes(lmms, Properties.Resources.LMMSPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = lmms;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать OBS? (yes/Enter - no)"); //OBS
            string unobs = Console.ReadLine();
            if (unobs == "yes")
            {
                string obs = patch + @"\OBSPortable.exe";
                File.WriteAllBytes(obs, Properties.Resources.OBSPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = obs;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать QMMP? (yes/Enter - no)"); //QMMP
            string unqmmp = Console.ReadLine();
            if (unqmmp == "yes")
            {
                string qmmp = patch + @"\QmmpPortable.exe";
                File.WriteAllBytes(qmmp, Properties.Resources.QmmpPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = qmmp;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать VLC? (yes/Enter - no)"); //VLC
            string unvlc = Console.ReadLine();
            if (unvlc == "yes")
            {
                string vlc = patch + @"\VLCPortable.exe";
                File.WriteAllBytes(vlc, Properties.Resources.VLCPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = vlc;
                p.Start(); //распаковка архива
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Раздел: Интернет");
            Console.ResetColor();

            Console.WriteLine("Распаковать FileZilla Client? (yes/Enter - no)"); //FileZilla
            string unfilezilla = Console.ReadLine();
            if (unfilezilla == "yes")
            {
                string filezilla = patch + @"\FileZillaPortable.exe";
                File.WriteAllBytes(filezilla, Properties.Resources.FileZillaPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = filezilla;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Firefox? (yes/Enter - no)"); //FIREFOX
            string unfirefox = Console.ReadLine();
            if (unfirefox == "yes")
            {
                string firefox = patch + @"\FirefoxPortable.exe";
                File.WriteAllBytes(firefox, Properties.Resources.FirefoxPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = firefox;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Pidgin? (yes/Enter - no)"); //Pidgin
            string unpidgin = Console.ReadLine();
            if (unpidgin == "yes")
            {
                string firefox = patch + @"\PidginPortable.exe";
                File.WriteAllBytes(firefox, Properties.Resources.PidginPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = firefox;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать qbittorrent? (yes/Enter - no)"); //qbittorrent
            string unqbit = Console.ReadLine();
            if (unqbit == "yes")
            {
                string qbit = patch + @"\qBittorrentPortable.exe";
                File.WriteAllBytes(qbit, Properties.Resources.qBittorrentPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = qbit;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Mozilla Thunderbird? (yes/Enter - no)"); //Thunderbird
            string unthunder = Console.ReadLine();
            if (unthunder == "yes")
            {
                string thunder = patch + @"\ThunderbirdPortable.exe";
                File.WriteAllBytes(thunder, Properties.Resources.ThunderbirdPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = thunder;
                p.Start(); //распаковка архива
            }


            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Раздел: Утилиты");
            Console.ResetColor();

            Console.WriteLine("Распаковать 7-Zip? (yes/Enter - no)"); //7ZIP
            string unzip = Console.ReadLine();
            if (unzip == "yes")
            {
                string zip = patch + @"\7-ZipPortable.exe";
                File.WriteAllBytes(zip, Properties.Resources._7_ZipPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = zip;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Areca? (yes/Enter - no)"); //ARECA
            string unareca = Console.ReadLine();
            if (unareca == "yes")
            {
                string areca = patch + @"\areca.exe";
                File.WriteAllBytes(areca, Properties.Resources.areca); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = areca;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать BleachBit? (yes/Enter - no)"); //BleachBit
            string unbleachbit = Console.ReadLine();
            if (unbleachbit == "yes")
            {
                string bleachbit = patch + @"\bleachbit.exe";
                File.WriteAllBytes(bleachbit, Properties.Resources.bleachbit); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = bleachbit;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать ClamWin? (yes/Enter - no)"); //ClamWin
            string unclamwin = Console.ReadLine();
            if (unclamwin == "yes")
            {
                string clamwin = patch + @"\ClamWinPortable.exe";
                File.WriteAllBytes(clamwin, Properties.Resources.ClamWinPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = clamwin;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Double Commander? (yes/Enter - no)"); //DOUBLE
            string undouble = Console.ReadLine();
            if (undouble == "yes")
            {
                string doublecmd = patch + @"\doublecmd.exe";
                File.WriteAllBytes(doublecmd, Properties.Resources.doublecmd); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = doublecmd;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать InfraRecorder? (yes/Enter - no)"); //INFRA
            string uninfra = Console.ReadLine();
            if (uninfra == "yes")
            {
                string infra = patch + @"\infrarecorder.exe";
                File.WriteAllBytes(infra, Properties.Resources.infrarecorder); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = infra;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать PeaZip? (yes/Enter - no)"); //PeaZip
            string unpea = Console.ReadLine();
            if (unpea == "yes")
            {
                string pea = patch + @"\PeaZipPortable.exe";
                File.WriteAllBytes(pea, Properties.Resources.PeaZipPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = pea;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Process Hacker? (yes/Enter - no)"); //PHacker
            string unprocess = Console.ReadLine();
            if (unprocess == "yes")
            {
                string process = patch + @"\ProcessHackerPortable.exe";
                File.WriteAllBytes(process, Properties.Resources.ProcessHackerPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = process;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать Rufus? (yes/Enter - no)"); //Rufus
            string unrufus = Console.ReadLine();
            if (unrufus == "yes")
            {
                string rufus = patch + @"\Rufus.exe";
                File.WriteAllBytes(rufus, Properties.Resources.Rufus); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = rufus;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать SnappyDriverInstaller? (yes/Enter - no)"); //SDI
            string unsdi = Console.ReadLine();
            if (unsdi == "yes")
            {
                string sdi = patch + @"\SDI_R1904.exe";
                File.WriteAllBytes(sdi, Properties.Resources.SDI_R1904); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = sdi;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать TightVNCViewer? (yes/Enter - no)"); //TightVNCViewer
            string unvnc = Console.ReadLine();
            if (unvnc == "yes")
            {
                string vnc = patch + @"\TightVNCViewerPortable.exe";
                File.WriteAllBytes(vnc, Properties.Resources.TightVNCViewerPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = vnc;
                p.Start(); //распаковка архива
            }

            Console.WriteLine("Распаковать UltraDefrag? (yes/Enter - no)"); //UltraDefragPortable
            string undefr = Console.ReadLine();
            if (undefr == "yes")
            {
                string defr = patch + @"\UltraDefragPortable.exe";
                File.WriteAllBytes(defr, Properties.Resources.UltraDefragPortable); //перенос файла              
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = defr;
                p.Start(); //распаковка архива
            }




        }
    }
}
